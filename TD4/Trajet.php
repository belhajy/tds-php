<?php

require_once 'Modele/ConnexionBaseDeDonnees.php';
require_once 'Modele/ModeleUtilisateur.php';

class Trajet {

    private ?int $id;
    private string $depart;
    private string $arrivee;
    private DateTime $date;
    private int $nbPlaces;
    private int $prix;
    private ModeleUtilisateur $conducteur;
    private bool $nonFumeur;

    /**
     * @var ModeleUtilisateur[]
     */
    private array $passagers;

    public function __construct(
        ?int              $id,
        string            $depart,
        string            $arrivee,
        DateTime          $date,
        int               $nbPlaces,
        int               $prix,
        ModeleUtilisateur $conducteur,
        bool              $nonFumeur
    )
    {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrivee = $arrivee;
        $this->date = $date;
        $this->nbPlaces = $nbPlaces;
        $this->prix = $prix;
        $this->conducteur = $conducteur;
        $this->nonFumeur = $nonFumeur;
        $this->passagers = [];
    }

    public static function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]),
            $trajetTableau["nbPlaces"],
            $trajetTableau["prix"],
            ModeleUtilisateur::getUtilisateurParLogin($trajetTableau["conducteurLogin"]),
            $trajetTableau["nonFumeur"],
        );
        $passagers = $trajet->recupererPassagers();
        $trajet->setPassagers($passagers);
        return $trajet;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getDepart(): string
    {
        return $this->depart;
    }

    public function setDepart(string $depart): void
    {
        $this->depart = $depart;
    }

    public function getArrivee(): string
    {
        return $this->arrivee;
    }

    public function setArrivee(string $arrivee): void
    {
        $this->arrivee = $arrivee;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    public function getPrix(): int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): void
    {
        $this->prix = $prix;
    }

    public function getConducteur(): ModeleUtilisateur
    {
        return $this->conducteur;
    }

    public function setConducteur(ModeleUtilisateur $conducteur): void
    {
        $this->conducteur = $conducteur;
    }

    public function isNonFumeur(): bool
    {
        return $this->nonFumeur;
    }

    public function setNonFumeur(bool $nonFumeur): void
    {
        $this->nonFumeur = $nonFumeur;
    }

    public function getNbPlaces(): int
    {
        return $this->nbPlaces;
    }

    public function setNbPlaces(int $nbPlaces): void
    {
        $this->nbPlaces = $nbPlaces;
    }

    public function getPassagers(): array
    {
        return $this->passagers;
    }

    public function setPassagers(array $passagers): void
    {
        $this->passagers = $passagers;
    }



    public function __toString()
    {
        $nonFumeur = $this->nonFumeur ? " non fumeur" : " ";
        return "<p>
            Le trajet$nonFumeur du {$this->date->format("d/m/Y")} partira de {$this->depart} pour aller à {$this->arrivee} (conducteur: {$this->conducteur->getPrenom()} {$this->conducteur->getNom()}).
        </p>";
    }

    /**
     * @return Trajet[]
     */
    public static function getTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajets[] = Trajet::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }

    public function ajouter(): bool
    {
        try {
            $pdo = ConnexionBaseDeDonnees::getPDO();
            $pdoStatement = $pdo->prepare("INSERT INTO trajet (depart, arrivee, date, nbPlaces, prix, conducteurLogin, nonFumeur) VALUES (:depart, :arrivee, :date, :nbPlaces, :prix, :conducteurLogin, :nonFumeur)");
            $values = array(
                "depart" => $this->depart,
                "arrivee" => $this->arrivee,
                "date" => $this->date->format("Y-m-d"),
                "nbPlaces" => $this->nbPlaces,
                "prix" => $this->prix,
                "conducteurLogin" => $this->conducteur->getLogin(),
                "nonFumeur" => $this->nonFumeur,
            );
            $pdoStatement->execute($values);

            $this->id = $pdo->lastInsertId();

            return true;
        } catch (PDOException $e) {
            return false;
        }
    }

    /**
     * @return ModeleUtilisateur[]
     */
    private function recupererPassagers() : array {
        $pdo = ConnexionBaseDeDonnees::getPDO();

        $sql = "SELECT utilisateur.* 
            FROM utilisateur 
            INNER JOIN passager ON passager.passagerLogin = utilisateur.login 
            WHERE passager.trajetId = :trajetId";

        $pdoStatement = $pdo->prepare($sql);

        $pdoStatement->execute(array('trajetId' => $this->id));

        $passagers = [];

        foreach ($pdoStatement as $passagerFormatTableau) {
            $passagers[] = ModeleUtilisateur::construireDepuisTableauSQL($passagerFormatTableau);
        }

        return $passagers;
    }

    public function supprimerPassager(string $passagerLogin): bool {
        $pdo = ConnexionBaseDeDonnees::getPDO();

        $sql = "DELETE FROM passager WHERE trajetId = :trajetId AND passagerLogin = :passagerLogin";

        $pdoStatement = $pdo->prepare($sql);

        $values = array(
            "trajetId" => $this->id,
            "passagerLogin" => $passagerLogin,
        );

        $pdoStatement->execute($values);

        return $pdoStatement->rowCount() > 0;
    }

    public static function getTrajetParLogin(string $login) : ?Trajet {
        $sql = "SELECT * from trajet WHERE id = :idTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "idTag" => $login,
        );

        $pdoStatement->execute($values);

        $trajetFormatTableau = $pdoStatement->fetch();
        if($trajetFormatTableau === false){
            return null;
        }
        return Trajet::construireDepuisTableauSQL($trajetFormatTableau);
    }

}
