<?php
require_once ('../Modele/ModeleUtilisateur.php'); // chargement du modèle
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::getUtilisateurs(); //appel au modèle pour gérer la BD
        self::afficherVue('utilisateur/liste.php', ['utilisateurs' => $utilisateurs]); //appel à la vue
    }

    public static function afficherDetail() : void {
        if(!isset($_GET['login'])){
            self::afficherVue('utilisateur/erreur.php');
            return;
        }
        $login = $_GET['login'];
        $utilisateur = ModeleUtilisateur::getUtilisateurParLogin($login);
        if($utilisateur === null){
            self::afficherVue('utilisateur/erreur.php');
        } else {
            self::afficherVue('utilisateur/detail.php', ['utilisateur' => $utilisateur]);
        };
    }

    public static function afficherFormulaireCreation() : void {
        self::afficherVue('utilisateur/formulaireUtilisateur.php');
    }

    public static function creerDepuisFormulaire() : void {
        if(!isset($_GET['login']) || !isset($_GET['nom']) || !isset($_GET['prenom'])){
            self::afficherVue('utilisateur/erreur.php');
            return;
        }
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];
        $utilisateur = new ModeleUtilisateur($login, $nom, $prenom);
        $utilisateur->ajouter();
        self::afficherListe();
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../vue/$cheminVue"; // Charge la vue
    }
}
?>
