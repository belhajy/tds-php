<?php
/** @var Trajet $trajet */
/** @var array $passagers */

use App\Covoiturage\Modele\DataObject\Trajet;

echo '<h1>Détail du trajet</h1>';
echo '<p>Trajet de ' . htmlspecialchars($trajet->getDepart()) . ' à ' . htmlspecialchars($trajet->getArrivee()) . '</p>';

echo '<h2>Passagers</h2>';
echo '<ul>';
if(empty($passagers)) {
    echo '<li>Aucun passager inscrit pour le moment.</li>';
} else {
    foreach ($passagers as $passager) {
        echo '<li>' . htmlspecialchars($passager->getNom()) . ' ' . htmlspecialchars($passager->getPrenom()) . ' (' . htmlspecialchars($passager->getLogin()) . ') ';
        echo '<a href="controleurFrontal.php?controleur=passager&action=desinscrire&login=' . urlencode($passager->getLogin()) . '&trajetId=' . urlencode($trajet->getId()) . '">Désinscrire</a></li>';
    }
    echo '</ul>';
}

//var_dump($trajet);
$idHTML = htmlspecialchars($trajet->getId());

//echo '<h2>Inscrire un passager</h2>';
//echo '<form action="controleurFrontal.php" method="get">';
//echo '<input type="hidden" name="action" value="inscrire">';
//echo '<input type="hidden" name="controleur" value="passager"';
//echo '<input type="hidden" name="trajetId" value="' . $idHTML . '">';
//echo '<label for="login">Login du passager:</label>';
//echo '<input type="text" id="login" name="login">';
//echo '<button type="submit">Inscrire</button>';
//echo '</form>';
//
//
//echo <<< HTML
//<h2>Inscrire un passager</h2>
//<form action="controleurFrontal.php" method="get">
//    <input type="hidden" name="action" value="inscrire">
//    <input type="hidden" name="controleur" value="passager"
//    <input type="hidden" name="trajetId" value="$idHTML">
//    <label for="login">Login du passager:</label>
//    <input type="text" id="login" name="login">
//    <button type="submit">Inscrire</button>
//</form>
//HTML;


?>

<h2>Inscrire un passager</h2>
<form action="controleurFrontal.php" method="get">
    <input type="hidden" name="action" value="inscrire">
    <input type="hidden" name="controleur" value="passager">
    <input type="hidden" name="trajetId" value="<?= htmlspecialchars($trajet->getId()) ?>">
    <label for="login">Login du passager:</label>
    <input type="text" id="login" name="login">
    <button type="submit">Inscrire</button>
</form>
