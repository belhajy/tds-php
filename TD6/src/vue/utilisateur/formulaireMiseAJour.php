<?php
/** @var Utilisateur $utilisateur */

use App\Covoiturage\Modele\DataObject\Utilisateur;

?>
<form method="get" action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input readonly class="InputAddOn-field" type="text" value= "<?= htmlspecialchars($utilisateur->getLogin()) ?>" name="login" id="login_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?= htmlspecialchars($utilisateur->getNom()) ?>" name="nom" id="nom_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prenom&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?= htmlspecialchars($utilisateur->getPrenom()) ?>" name="prenom" id="prenom_id" required/>
        </p>
        <input type='hidden' name='action' value='mettreAJour'>
        <p>
            <input class="InputAddOn-field" type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>
