<?php
/** @var Utilisateur $utilisateur */

use App\Covoiturage\Modele\DataObject\Utilisateur;

echo '<p> Utilisateur ' . htmlspecialchars($utilisateur->getNom()) . ' ' . htmlspecialchars($utilisateur->getPrenom()) . ' de login ' . htmlspecialchars($utilisateur->getLogin()) .'.</p>';

