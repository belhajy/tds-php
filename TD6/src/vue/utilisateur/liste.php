<a href="controleurFrontal.php?action=afficherFormulaireCreation">Créer un utilisateur</a>
<?php
/** @var \App\Covoiturage\Modele\DataObject\Utilisateur[] $utilisateurs */
foreach ($utilisateurs as $utilisateur) {
    echo '<p> Utilisateur ' . htmlspecialchars($utilisateur->getNom()) . ' ' . htmlspecialchars($utilisateur->getPrenom()) . ' de login 
    <a href="controleurFrontal.php?action=afficherDetail&login=' . rawurlencode($utilisateur->getLogin()) . '">' . htmlspecialchars($utilisateur->getLogin()) . '</a>.
    <a href="controleurFrontal.php?action=afficherFormulaireMiseAJour&login=' . rawurlencode($utilisateur->getLogin()) . '">' . "Modifier" . '</a>.
    <a href="controleurFrontal.php?action=supprimer&login=' . rawurlencode($utilisateur->getLogin()) . '">' . "Supprimer" . '</a>.</p>';
}
?>
