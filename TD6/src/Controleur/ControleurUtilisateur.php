<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        ; //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php',
            ["titre" => "Liste des utilisateurs",
            "cheminCorpsVue" => "utilisateur/liste.php",
                'utilisateurs' => $utilisateurs]); //appel à la vue
    }

    public static function afficherDetail() : void {
        if(!isset($_GET['login'])){
            self::afficherErreur("Erreur avec l'utilisateur " . $_GET['login']);
            return;
        }
        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if($utilisateur === null){
            self::afficherErreur("Utilisateur " . $login . " non trouvé");
        } else {
            self::afficherVue('vueGenerale.php',
                ["titre" => "Detail de l'utilisateur " .$login,
                    "cheminCorpsVue" => "utilisateur/detail.php",
                    'utilisateur' => $utilisateur]);
        };
    }

    public static function afficherFormulaireCreation() : void {
        self::afficherVue('vueGenerale.php',
            ["titre" => "Formulaire création utilisateur",
            "cheminCorpsVue" => "utilisateur/formulaireUtilisateur.php",]);
    }

    public static function creerDepuisFormulaire() : void {
        if(!isset($_GET['login']) || !isset($_GET['nom']) || !isset($_GET['prenom'])){
            self::afficherErreur("Paramètres manquants");
            return;
        }
        $utilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository())->ajouter($utilisateur);
        self::afficherVue('vueGenerale.php', [
            "titre" => "Utilisateur créé",
            "cheminCorpsVue" => "utilisateur/utilisateurCree.php",
            "utilisateurs" => (new UtilisateurRepository())->recuperer(),
        ]);
    }

    public static function afficherErreur(string $messageErreur = "") {
        if(empty($messageErreur)){
            self::afficherVue('vueGenerale.php', [
                "titre" => "Erreur",
                "cheminCorpsVue" => "utilisateur/erreur.php",
                "messageErreur" => "Problème méthode",
            ]);
        } else {
            self::afficherVue('vueGenerale.php', [
                "titre" => "Erreur",
                "cheminCorpsVue" => "utilisateur/erreur.php",
                "messageErreur" => $messageErreur,
            ]);
        }
    }

    public static function supprimer()
    {
        if(!isset($_GET['login'])){
            self::afficherErreur("Erreur avec l'utilisateur " . $_GET['login']);
            return;
        }
        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if($utilisateur === null){
            self::afficherErreur("Utilisateur " . $login . " non trouvé");
        } else {
            (new UtilisateurRepository())->supprimer(["login" => $login]);
            self::afficherVue('vueGenerale.php', [
                "titre" => "Utilisateur supprimé",
                "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php",
                "login" => $login,
                "utilisateurs" => (new UtilisateurRepository())->recuperer(),
            ]);
        }
    }

    public static function afficherFormulaireMiseAJour()
    {
        if(!isset($_GET['login'])){
            self::afficherErreur("Erreur avec l'utilisateur " . $_GET['login']);
            return;
        }
        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if($utilisateur === null){
            self::afficherErreur("Utilisateur " . $login . " non trouvé");
        } else {
            self::afficherVue('vueGenerale.php', [
                "titre" => "Mise à jour de l'utilisateur " . $login,
                "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php",
                "utilisateurs" => $utilisateur,
            ]);
        }
    }

    public static function mettreAJour() {
        if(!isset($_GET['login']) || !isset($_GET['nom']) || !isset($_GET['prenom'])){
            self::afficherErreur("Paramètres manquants");
            return;
        }
        $utilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository())->mettreAJour($utilisateur);
        self::afficherVue('vueGenerale.php', [
            "titre" => "Utilisateur mis à jour",
            "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php",
            "login" => $login,
            "utilisateurs" => (new UtilisateurRepository())->recuperer(),
        ]);
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require_once __DIR__ .  "/../vue/$cheminVue"; // Charge la vue
    }

    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $login = $tableauDonneesFormulaire['login'];
        $nom = $tableauDonneesFormulaire['nom'];
        $prenom = $tableauDonneesFormulaire['prenom'];
        return new Utilisateur($login, $nom, $prenom);
    }
}
?>
