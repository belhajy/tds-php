<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use DateTime;

class ControleurTrajet
{
    public static function afficherListe() : void {
        $trajets = (new TrajetRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php',
            ["titre" => "Liste des trajets",
                "cheminCorpsVue" => "trajet/liste.php",
                'trajets' => $trajets]); //appel à la vue
    }

    public static function afficherDetail() : void
    {
        if (!isset($_GET['id'])) {
            self::afficherErreur("Erreur avec le trajet " . $_GET['id']);
            return;
        }
        $id = $_GET['id'];
        $trajet = (new TrajetRepository())->recupererParClePrimaire($id);
        $passagers = (new TrajetRepository())->recupererPassagers($id);
        if ($trajet === null) {
            self::afficherErreur("Trajet " . $id . " non trouvé");
        } else {
            self::afficherVue('vueGenerale.php',
                ["titre" => "Detail du trajet " . $id,
                    "cheminCorpsVue" => "trajet/detail.php",
                    'trajet' => $trajet,
                    'passagers' => $passagers]);
        };
    }

    public static function afficherFormulaireCreation()
    {
        self::afficherVue('vueGenerale.php',
            ["titre" => "Création d'un trajet",
                "cheminCorpsVue" => "trajet/formulaireTrajet.php",
                "conducteurs" => (new UtilisateurRepository())->recuperer()]);
    }

    public static function creerDepuisFormulaire()
    {
        if (!isset($_GET['depart']) || !isset($_GET['arrivee']) || !isset($_GET['date']) || !isset($_GET['nbPlaces']) || !isset($_GET['prix']) || !isset($_GET['conducteurLogin'])) {
            self::afficherErreur("Erreur avec le trajet " . $_GET['id']);
            return;
        }
        $trajet = self::construireDepuisFormulaire($_GET);
        (new TrajetRepository())->ajouter($trajet);
        self::afficherVue('vueGenerale.php',
            ["titre" => "Trajet créé",
                "cheminCorpsVue" => "trajet/trajetCree.php",
                "trajets" => (new TrajetRepository())->recuperer()]);
    }

    public static function afficherFormulaireMiseAJour()
    {
        if (!isset($_GET['id'])) {
            self::afficherErreur("Erreur avec le trajet " . $_GET['id']);
            return;
        }
        $id = $_GET['id'];
        $trajet = (new TrajetRepository())->recupererParClePrimaire($id);;
        if ($trajet === null) {
            self::afficherErreur("Trajet " . $id . " non trouvé");
        } else {
            self::afficherVue('vueGenerale.php',
                ["titre" => "Mise à jour du trajet " . $id,
                    "cheminCorpsVue" => "trajet/formulaireMiseAJour.php",
                    'trajet' => $trajet,
                    "conducteurs" => (new UtilisateurRepository())->recuperer()]);
        }
    }

    public static function mettreAJour()
    {
        if (!isset($_GET['id']) || !isset($_GET['depart']) || !isset($_GET['arrivee']) || !isset($_GET['date']) || !isset($_GET['nbPlaces']) || !isset($_GET['prix']) || !isset($_GET['conducteurLogin'])) {
            self::afficherErreur("Erreur avec le trajet " . $_GET['id']);
            return;
        }
        $trajet = self::construireDepuisFormulaire($_GET);
        (new TrajetRepository())->mettreAJour($trajet);
        self::afficherVue('vueGenerale.php',
            ["titre" => "Trajet mis à jour",
                "cheminCorpsVue" => "trajet/trajetMiseAJour.php",
                'id' => $trajet->getId(),
                "trajets" => (new TrajetRepository())->recuperer()]);
    }

    public static function supprimer() : void
    {
        if (!isset($_GET['id'])) {
            self::afficherErreur("Erreur avec le trajet " . $_GET['id']);
            return;
        }
        $id = $_GET['id'];
        (new TrajetRepository())->supprimer(["id" => $id]);
        self::afficherVue('vueGenerale.php',
            ["titre" => "Trajet supprimé",
                "cheminCorpsVue" => "trajet/trajetSupprime.php",
                'id' => $id,
                "trajets" => (new TrajetRepository())->recuperer()]);
    }

    public static function afficherErreur(string $messageErreur = "") {
        if(empty($messageErreur)){
            self::afficherVue('vueGenerale.php', [
                "titre" => "Erreur",
                "cheminCorpsVue" => "utilisateur/erreur.php",
                "messageErreur" => "Problème méthode",
            ]);
        } else {
            self::afficherVue('vueGenerale.php', [
                "titre" => "Erreur",
                "cheminCorpsVue" => "utilisateur/erreur.php",
                "messageErreur" => $messageErreur,
            ]);
        }
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require_once __DIR__ .  "/../vue/$cheminVue"; // Charge la vue
    }

    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Trajet
    {
        $id = $tableauDonneesFormulaire["id"] ?? null;
        $depart = $tableauDonneesFormulaire['depart'];
        $arrivee = $tableauDonneesFormulaire['arrivee'];
        $date = new DateTime($tableauDonneesFormulaire['date']);
        $nbPlaces = $tableauDonneesFormulaire['nbPlaces'];
        $prix = $tableauDonneesFormulaire['prix'];
        $conducteurLogin = (new UtilisateurRepository())->recupererParClePrimaire($tableauDonneesFormulaire['conducteurLogin']);
        $nonFumeur = isset($tableauDonneesFormulaire['nonFumeur']) ? 1 : 0;
        return new Trajet($id, $depart, $arrivee, $date, $nbPlaces, $prix, $conducteurLogin, $nonFumeur);
    }

}