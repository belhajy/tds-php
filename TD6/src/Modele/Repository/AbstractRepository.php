<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use PDOException;

abstract class AbstractRepository
{

    protected abstract function getNomTable(): string;

    protected abstract function getNomClePrimaire(): string;
    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau) : AbstractDataObject;
    /** @return string[] */
    protected abstract function getNomsColonnes(): array;
    protected abstract function formatTableauSQL(AbstractDataObject $trajet): array;





    public function ajouter(AbstractDataObject $objet): bool
    {
        try {
            $pdo = ConnexionBaseDeDonnees::getPDO();
            $nomTags = array();
            foreach ($this->getNomsColonnes() as $nomColonne) {
                $nomTags[] = $nomColonne . "Tag";
            }
            $sql = "INSERT INTO " . $this->getNomTable() . " (" . implode(", ", $this->getNomsColonnes()) . ") VALUES (:" . implode(", :", $nomTags) . ");";
            $pdoStatement = $pdo->prepare($sql);;
            $values = $this->formatTableauSQL($objet);
            $pdoStatement->execute($values);
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }

    public function mettreAJour(AbstractDataObject $object): bool
    {
        try {
            $pdo = ConnexionBaseDeDonnees::getPDO();
            $nomTags = array();
            foreach ($this->getNomsColonnes() as $nomColonne) {
                $nomTags[] = $nomColonne . " = :" . $nomColonne . "Tag";
            }
            $sql = "UPDATE " . $this->getNomTable() . " SET " . implode(", ", $nomTags) . " WHERE " . $this->getNomClePrimaire() . " = :" . $this->getNomClePrimaire() . "Tag";
            $pdoStatement = $pdo->prepare($sql);
            $values = $this->formatTableauSQL($object);
            $pdoStatement->execute($values);
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }

    /**
     * @return AbstractDataObject[]
     */
    public function recuperer(): array
    {
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM " . $this->getNomTable());
        $dataObject = array();
        foreach ($pdoStatement as $objectFormatTableau) {
            $dataObject[] = $this->construireDepuisTableauSQL($objectFormatTableau);
        }
        return $dataObject;
    }

    public function recupererParClePrimaire(string $valeurClePrimaire): ?AbstractDataObject
    {
        $sql = "SELECT * from " . $this->getNomTable() . " WHERE " . $this->getNomClePrimaire() . " = :valeurClePrimaireTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "valeurClePrimaireTag" => $valeurClePrimaire,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $objetFormatTableau = $pdoStatement->fetch();
        if ($objetFormatTableau === false) {
            return null;
        }
        return $this->construireDepuisTableauSQL($objetFormatTableau);
    }

    public function supprimer(array $valeursClePrimaire)
    {
        $pdo = ConnexionBaseDeDonnees::getPDO();
        $conditions = [];
        foreach ($valeursClePrimaire as $cle => $valeur) {
            $conditions[] = "$cle = :$cle";
        }
        $whereClause = implode(' AND ', $conditions);

        $pdoStatement = $pdo->prepare("DELETE FROM " . $this->getNomTable() . " WHERE " . $whereClause);
        $pdoStatement->execute($valeursClePrimaire);
    }


}