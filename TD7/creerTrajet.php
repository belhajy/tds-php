<?php
require_once "Trajet.php";
$depart = $_POST['depart'];
$arrivee = $_POST['arrivee'];
$date = $_POST['date'];
$nbPlaces = $_POST['nbPlaces'];
$prix = $_POST['prix'];
$conducteurLogin = $_POST['conducteurLogin'];
$nonFumeur = $_POST['nonFumeur'];
if(!empty($depart) && !empty($arrivee) && !empty($date) && !empty($prix) && !empty($conducteurLogin) && !empty($nonFumeur)){
    $trajet = new Trajet(null, $depart, $arrivee, new DateTime($date), $nbPlaces, $prix, ModeleUtilisateur::getUtilisateurParLogin($conducteurLogin), $nonFumeur);
    $trajet->ajouter();
}
else{
    echo " Une des valeurs est vide";
}