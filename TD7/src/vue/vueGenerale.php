<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title><?php echo $titre; ?></title>
    <link rel="stylesheet" href="../ressources/css/style.css">
</head>
<body>

<header>
    <nav>
        <!-- Votre menu de navigation ici -->
        <ul>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
            </li>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
            </li>
            <li>
                <a href="controleurFrontal.php?action=afficherFormulairePreference"><img src="../ressources/images/heart.png"></a>
            </li>
        </ul>
    </nav>
</header>
<main>
    <?php
    require __DIR__ . "/{$cheminCorpsVue}";
    ?>
</main>
<footer>
    <p>
        Site de covoiturage de Belhaj Yaniss
    </p>
</footer>
</body>
</html>

