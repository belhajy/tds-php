<a href="controleurFrontal.php?controleur=trajet&action=afficherFormulaireCreation">Créer un trajet</a>
<?php
/** @var Trajet[] $trajets */

use App\Covoiturage\Modele\DataObject\Trajet;

foreach ($trajets as $trajet) {
    echo '<p> Trajet qui part de ' . htmlspecialchars($trajet->getDepart()) . ' en destination de ' . htmlspecialchars($trajet->getArrivee()) . ' d identifiant 
    <a href="controleurFrontal.php?controleur=trajet&action=afficherDetail&id=' . rawurlencode($trajet->getId()) . '">' . htmlspecialchars($trajet->getId()) . '</a>.
    <a href="controleurFrontal.php?controleur=trajet&action=afficherFormulaireMiseAJour&id=' . rawurlencode($trajet->getId()) . '">' . "Modifier" . '</a>.
    <a href="controleurFrontal.php?controleur=trajet&action=supprimer&id=' . rawurlencode($trajet->getId()) . '">' . "Supprimer" . '</a>.</p>';
}
?>