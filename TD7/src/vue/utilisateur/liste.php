<a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireCreation">Créer un utilisateur</a>
<?php
/** @var Utilisateur[] $utilisateurs */

use App\Covoiturage\Modele\DataObject\Utilisateur;

foreach ($utilisateurs as $utilisateur) {
    echo '<p> Utilisateur ' . htmlspecialchars($utilisateur->getNom()) . ' ' . htmlspecialchars($utilisateur->getPrenom()) . ' de login 
    <a href="controleurFrontal.php?controleur=utilisateur&action=afficherDetail&login=' . rawurlencode($utilisateur->getLogin()) . '">' . htmlspecialchars($utilisateur->getLogin()) . '</a>.
    <a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login=' . rawurlencode($utilisateur->getLogin()) . '">' . "Modifier" . '</a>.
    <a href="controleurFrontal.php?controleur=utilisateur&action=supprimer&login=' . rawurlencode($utilisateur->getLogin()) . '">' . "Supprimer" . '</a>.</p>';
}
?>
