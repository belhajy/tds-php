<?php
use App\Covoiturage\Lib\PreferenceControleur;

$preference = PreferenceControleur::existe() ? PreferenceControleur::lire() : '';
?>

<form action="controleurFrontal.php" method="get">
    <fieldset>
        <legend>Choisissez votre contrôleur par défaut</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="utilisateurId">Utilisateur</label>
            <input class="InputAddOn-field" type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur" <?php echo $preference === 'utilisateur' ? 'checked' : ''; ?>>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="trajetId">Trajet</label>
            <input class="InputAddOn-field" type="radio" id="trajetId" name="controleur_defaut" value="trajet" <?php echo $preference === 'trajet' ? 'checked' : ''; ?>>
        </p>
        <input type='hidden' name='action' value='enregistrerPreference'>
        <input type='hidden' name='controleur' value='generique'>
        <p class="InputAddOn">
            <button class="InputAddOn-field" type="submit">Enregistrer</button>
        </p>
    </fieldset>
</form>