<?php

namespace App\Covoiturage\Configuration;

class ConfigurationSite
{
    static private array $configurationSite = array(

        'sessionExpiration' => 1800,
    );

    static public function getSessionExpiration() : string {
        return ConfigurationSite::$configurationSite['sessionExpiration'];
    }
}