<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\Passager;
use App\Covoiturage\Modele\Repository\PassagerRepository;

class ControleurPassager extends ControleurGenerique
{
    public static function inscrire() : void {
        if (!isset($_GET['login']) || !isset($_GET['trajetId'])) {
            self::afficherErreur("Paramètres manquants");
            return;
        }
        $login = $_GET['login'];
        $trajetId = $_GET['trajetId'];
        $passager = new Passager($trajetId, $login);
        (new PassagerRepository())->ajouter($passager);
        self::afficherVue('vueGenerale.php',
            ["titre" => "Ajout du passage dans  " . $trajetId,
                "cheminCorpsVue" => "passager/passagerAjouter.php",
                'trajetId' => $trajetId,
                'login' => $login]);
    }

    public static function desinscrire() : void {
        if (!isset($_GET['login']) || !isset($_GET['trajetId'])) {
            self::afficherErreur("Paramètres manquants");
            return;
        }
        $login = $_GET['login'];
        $trajetId = $_GET['trajetId'];
        (new PassagerRepository())->supprimer(["passagerLogin" => $login, "trajetId" => $trajetId]);
        self::afficherVue('vueGenerale.php',
            ["titre" => "Suppression du passage dans  " . $trajetId,
                "cheminCorpsVue" => "passager/passagerSupprimer.php",
                'trajetId' => $trajetId,
                'login' => $login]);
    }

    private static function afficherErreur(string $messageErreur = "") {
        self::afficherVue('utilisateur/erreur.php', [
            "titre" => "Erreur",
            "messageErreur" => $messageErreur,
        ]);
    }
}