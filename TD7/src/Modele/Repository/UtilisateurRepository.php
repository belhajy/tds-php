<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use PDOException;

class UtilisateurRepository extends AbstractRepository
{


    /**
     * @return Trajet[]
     */
    public static function recupererTrajetsCommePassager(Utilisateur $utilisateur) : array {
        $sql = "SELECT * FROM trajet WHERE id IN (SELECT trajetId FROM passager WHERE passagerLogin = :loginTag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "loginTag" => $utilisateur->getLogin(),
        );
        $pdoStatement->execute($values);
        $trajets = array();
        foreach($pdoStatement as $trajetFormatTableau){
            $trajets[] = (new TrajetRepository())->construireDepuisTableauSQL($trajetFormatTableau);
        }
        return $trajets;
    }

    protected function construireDepuisTableauSQL(array $utilisateurFormatTableau) : Utilisateur {
        return new Utilisateur($utilisateurFormatTableau['login'], $utilisateurFormatTableau['nom'], $utilisateurFormatTableau['prenom']);
    }

    protected function getNomTable(): string
    {
        return "utilisateur";
    }

    protected function getNomClePrimaire(): string
    {
        return "login";
    }

    /** @return string[] */
    protected function getNomsColonnes(): array
    {
        return ["login", "nom", "prenom"];
    }

    protected function formatTableauSQL(AbstractDataObject $utilisateur): array
    {
        /** @var Utilisateur $utilisateur */
        return array(
            "loginTag" => $utilisateur->getLogin(),
            "nomTag" => $utilisateur->getNom(),
            "prenomTag" => $utilisateur->getPrenom(),
        );
    }


}