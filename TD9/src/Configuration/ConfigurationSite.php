<?php

namespace App\Covoiturage\Configuration;

class ConfigurationSite
{
    static private array $configurationSite = array(

        'sessionExpiration' => 1800,

        'URLAbsolue' => 'http://localhost/tds-php/TD8/web/controleurFrontal.php'
    );

    static public function getSessionExpiration() : string {
        return ConfigurationSite::$configurationSite['sessionExpiration'];
    }

    static public function getURLAbsolue() : string {
        return ConfigurationSite::$configurationSite['URLAbsolue'];
    }

    static public function getDebug() : bool
    {
        return true;
    }

}