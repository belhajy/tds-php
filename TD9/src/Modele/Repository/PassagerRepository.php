<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;

class PassagerRepository extends AbstractRepository
{

    protected function getNomTable(): string
    {
        return "passager";
    }

    protected function getNomClePrimaire(): string
    {
        return "trajetId, passagerLogin";
    }

    protected function construireDepuisTableauSQL(array $objetFormatTableau): AbstractDataObject
    {
        return new Passager($objetFormatTableau["trajetId"], $objetFormatTableau["passagerLogin"]);
    }

    protected function getNomsColonnes(): array
    {
        return ["trajetId", "passagerLogin"];
    }

    protected function formatTableauSQL(AbstractDataObject $passager): array
    {
        return [
            "trajetIdTag" => $passager->getTrajetId(),
            "passagerLoginTag" => $passager->getPassagerLogin()
        ];
    }
}