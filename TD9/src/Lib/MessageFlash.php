<?php

namespace App\Covoiturage\Lib;


use App\Covoiturage\Modele\HTTP\Session;

class MessageFlash
{

    // Les messages sont enregistrés en session associée à la clé suivante
    private static string $cleFlash = "_messagesFlash";

    // $type parmi "success", "info", "warning" ou "danger"
    public static function ajouter(string $type, string $message): void
    {
        $session = Session::getInstance();
        if (!$session->contient(self::$cleFlash)) {
            $session->enregistrer(self::$cleFlash, ["warning"=>[],"success"=>[],"danger"=>[],"info"=>[]]);
        }
        $messages=$session->lire(self::$cleFlash);
        $messages[$type][] = $message;

        Session::getInstance()->enregistrer(MessageFlash::$cleFlash,$messages);
    }

    public static function contientMessage(string $type): bool
    {
        return Session::getInstance()->lire(self::$cleFlash)[$type];
    }

    // Attention : la lecture doit détruire le message
    public static function lireMessages(string $type): array
    {
        $session=Session::getInstance();
        if($session->contient(self::$cleFlash)){
            $ToutMessages=$session->lire(self::$cleFlash);
            if(isset($ToutMessages[$type])){
                $messages=$ToutMessages[$type];
                $ToutMessages[$type]=array();
                $session->enregistrer(self::$cleFlash,$ToutMessages);
                return $messages;
            }
        }
        return [];
    }

    public static function lireTousMessages() : array
    {
        $groupeMessage=[];
        $session=Session::getInstance();
        if($session->contient(self::$cleFlash)){
            $messages=$session->lire(self::$cleFlash);
            foreach($messages as $type=>$tableau){
                $temp=self::lireMessages($type);
                $groupeMessage[$type] = $temp;
            }
            return $groupeMessage;
        }
        return [];
    }

}