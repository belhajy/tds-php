<?php

namespace App\Covoiturage\Lib;

use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ConnexionUtilisateur
{
    // L'utilisateur connecté sera enregistré en session associé à la clé suivante
    private static string $cleConnexion = "_utilisateurConnecte";

    public static function connecter(string $loginUtilisateur): void
    {
        $session = Session::getInstance();
        $session->enregistrer(self::$cleConnexion, $loginUtilisateur);
    }

    public static function estConnecte(): bool
    {
        $session = Session::getInstance();
        return $session->contient(self::$cleConnexion);
    }

    public static function deconnecter(): void
    {
        $session = Session::getInstance();
        $session->detruire();
    }

    public static function getLoginUtilisateurConnecte(): ?string
    {
        $session = Session::getInstance();
        return $session->lire(self::$cleConnexion) ?? null;
    }

    public static function estUtilisateur($login): bool {
        return self::getLoginUtilisateurConnecte() == $login;
    }

    public static function estAdministrateur() : bool {
        if(self::estConnecte()) {
            if (self::getLoginUtilisateurConnecte() !== null) {
                return (new UtilisateurRepository())->recupererParClePrimaire(self::getLoginUtilisateurConnecte())->isEstAdmin();
            }
        }
        return false;
    }


}

