<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\MessageFlash;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use DateTime;

class ControleurTrajet extends ControleurGenerique
{
    public static function afficherListe() : void {
        $trajets = (new TrajetRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php',
            ["titre" => "Liste des trajets",
                "cheminCorpsVue" => "trajet/liste.php",
                'trajets' => $trajets]); //appel à la vue
    }

    public static function afficherDetail() : void
    {
        if (!isset($_REQUEST['id'])) {
            MessageFlash::ajouter("danger", "Erreur avec le trajet " . $_REQUEST['id']);
            self::redirectionVersURL("controleurFrontal.php?action=afficherListe&controleur=trajet");
            return;
        }
        $id = $_REQUEST['id'];
        $trajet = (new TrajetRepository())->recupererParClePrimaire($id);
        $passagers = (new TrajetRepository())->recupererPassagers($id);
        if ($trajet === null) {
            MessageFlash::ajouter("danger", "Trajet " . $id . " non trouvé");
            self::redirectionVersURL("controleurFrontal.php?action=afficherListe&controleur=trajet");
        } else {
            self::afficherVue('vueGenerale.php',
                ["titre" => "Detail du trajet " . $id,
                    "cheminCorpsVue" => "trajet/detail.php",
                    'trajet' => $trajet,
                    'passagers' => $passagers]);
        };
    }

    public static function afficherFormulaireCreation()
    {
        self::afficherVue('vueGenerale.php',
            ["titre" => "Création d'un trajet",
                "cheminCorpsVue" => "trajet/formulaireTrajet.php",
                "conducteurs" => (new UtilisateurRepository())->recuperer()]);
    }

    public static function creerDepuisFormulaire()
    {
        if (!isset($_REQUEST['depart']) || !isset($_REQUEST['arrivee']) || !isset($_REQUEST['date']) || !isset($_REQUEST['nbPlaces']) || !isset($_REQUEST['prix']) || !isset($_REQUEST['conducteurLogin'])) {
            MessageFlash::ajouter("danger", "Erreur avec le trajet " . $_REQUEST['id']);
            self::redirectionVersURL("controleurFrontal.php?action=afficherFormulaireCreation&controleur=trajet");
            return;
        }
        $trajet = self::construireDepuisFormulaire($_REQUEST);
        (new TrajetRepository())->ajouter($trajet);
        MessageFlash::ajouter("success", "Trajet créé");
        self::redirectionVersURL("controleurFrontal.php?action=afficherListe&controleur=trajet");
    }

    public static function afficherFormulaireMiseAJour()
    {
        if (!isset($_REQUEST['id'])) {
            MessageFlash::ajouter("danger", "Erreur avec le trajet " . $_REQUEST['id']);
            self::redirectionVersURL("controleurFrontal.php?action=afficherListe&controleur=trajet");
            return;
        }
        $id = $_REQUEST['id'];
        $trajet = (new TrajetRepository())->recupererParClePrimaire($id);;
        if ($trajet === null) {
            MessageFlash::ajouter("danger", "Trajet " . $id . " non trouvé");
            self::redirectionVersURL("controleurFrontal.php?action=afficherListe&controleur=trajet");
        } else {
            self::afficherVue('vueGenerale.php',
                ["titre" => "Mise à jour du trajet " . $id,
                    "cheminCorpsVue" => "trajet/formulaireMiseAJour.php",
                    'trajet' => $trajet,
                    "conducteurs" => (new UtilisateurRepository())->recuperer()]);
        }
    }

    public static function mettreAJour()
    {
        if (!isset($_REQUEST['id']) || !isset($_REQUEST['depart']) || !isset($_REQUEST['arrivee']) || !isset($_REQUEST['date']) || !isset($_REQUEST['nbPlaces']) || !isset($_REQUEST['prix']) || !isset($_REQUEST['conducteurLogin'])) {
            MessageFlash::ajouter("danger", "Erreur avec le trajet " . $_REQUEST['id']);
            self::redirectionVersURL("controleurFrontal.php?action=afficherListe&controleur=trajet");
            return;
        }
        $trajet = self::construireDepuisFormulaire($_REQUEST);
        (new TrajetRepository())->mettreAJour($trajet);
        MessageFlash::ajouter("success", "Trajet mis à jour");
        self::redirectionVersURL("controleurFrontal.php?action=afficherListe&controleur=trajet");
    }

    public static function supprimer() : void
    {
        if (!isset($_REQUEST['id'])) {
            MessageFlash::ajouter("danger", "Erreur avec le trajet " . $_REQUEST['id']);
            self::redirectionVersURL("controleurFrontal.php?action=afficherListe&controleur=trajet");
            return;
        }
        $id = $_REQUEST['id'];
        (new TrajetRepository())->supprimer(["id" => $id]);
        MessageFlash::ajouter("success", "Trajet supprimé");
        self::redirectionVersURL("controleurFrontal.php?action=afficherListe&controleur=trajet");
    }

    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Trajet
    {
        $id = $tableauDonneesFormulaire["id"] ?? null;
        $depart = $tableauDonneesFormulaire['depart'];
        $arrivee = $tableauDonneesFormulaire['arrivee'];
        $date = new DateTime($tableauDonneesFormulaire['date']);
        $nbPlaces = $tableauDonneesFormulaire['nbPlaces'];
        $prix = $tableauDonneesFormulaire['prix'];
        $conducteurLogin = (new UtilisateurRepository())->recupererParClePrimaire($tableauDonneesFormulaire['conducteurLogin']);
        $nonFumeur = isset($tableauDonneesFormulaire['nonFumeur']) ? 1 : 0;
        return new Trajet($id, $depart, $arrivee, $date, $nbPlaces, $prix, $conducteurLogin, $nonFumeur);
    }

}