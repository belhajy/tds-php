<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\MessageFlash;
use App\Covoiturage\Modele\DataObject\Passager;
use App\Covoiturage\Modele\Repository\PassagerRepository;

class ControleurPassager extends ControleurGenerique
{
    public static function inscrire() : void {
        if (!isset($_REQUEST['login']) || !isset($_REQUEST['trajetId'])) {
            MessageFlash::ajouter("danger", "Paramètres manquants");
            self::redirectionVersURL("controleurFrontal.php?action=afficherListe&controleur=trajet");
            return;
        }
        $login = $_REQUEST['login'];
        $trajetId = $_REQUEST['trajetId'];
        $passager = new Passager($trajetId, $login);
        (new PassagerRepository())->ajouter($passager);
        self::afficherVue('vueGenerale.php',
            ["titre" => "Ajout du passage dans  " . $trajetId,
                "cheminCorpsVue" => "passager/passagerAjouter.php",
                'trajetId' => $trajetId,
                'login' => $login]);
    }

    public static function desinscrire() : void {
        if (!isset($_REQUEST['login']) || !isset($_REQUEST['trajetId'])) {
            MessageFlash::ajouter("danger", "Paramètres manquants");
            self::redirectionVersURL("controleurFrontal.php?action=afficherListe&controleur=trajet");
            return;
        }
        $login = $_REQUEST['login'];
        $trajetId = $_REQUEST['trajetId'];
        (new PassagerRepository())->supprimer(["passagerLogin" => $login, "trajetId" => $trajetId]);
        self::afficherVue('vueGenerale.php',
            ["titre" => "Suppression du passage dans  " . $trajetId,
                "cheminCorpsVue" => "passager/passagerSupprimer.php",
                'trajetId' => $trajetId,
                'login' => $login]);
    }
}