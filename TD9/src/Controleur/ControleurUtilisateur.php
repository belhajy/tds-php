<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Lib\MessageFlash;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Lib\VerificationEmail;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
class ControleurUtilisateur extends ControleurGenerique{

    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        ; //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php',
            ["titre" => "Liste des utilisateurs",
            "cheminCorpsVue" => "utilisateur/liste.php",
                'utilisateurs' => $utilisateurs]); //appel à la vue
    }

    public static function afficherDetail() : void {
        if(!isset($_REQUEST['login'])){
            self::redirectionVersURL("controleurFrontal.php?action=afficherListe&controleur=utilisateur");
            return;
        }
        $login = $_REQUEST['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if($utilisateur === null){
            MessageFlash::ajouter("danger", "Utilisateur inconnu");
            self::redirectionVersURL("controleurFrontal.php?action=afficherListe&controleur=utilisateur");
        } else {
            self::afficherVue('vueGenerale.php',
                ["titre" => "Detail de l'utilisateur " .$login,
                    "cheminCorpsVue" => "utilisateur/detail.php",
                    'utilisateur' => $utilisateur]);
        };
    }

    public static function afficherFormulaireCreation() : void {
        self::afficherVue('vueGenerale.php',
            ["titre" => "Formulaire création utilisateur",
            "cheminCorpsVue" => "utilisateur/formulaireUtilisateur.php",]);
    }

    public static function creerDepuisFormulaire() : void {
        if(!isset($_REQUEST['login']) || !isset($_REQUEST['nom']) || !isset($_REQUEST['prenom']) || !isset($_REQUEST['mdp']) || !isset($_REQUEST['mdp2']) || !isset($_REQUEST['email'])){
            MessageFlash::ajouter("danger", "Paramètres manquants");
            self::redirectionVersURL("controleurFrontal.php?action=afficherFormulaireCreation&controleur=utilisateur");
        }
        elseif(isset($_REQUEST['estAdmin']) && ($_REQUEST['estAdmin'] && !ConnexionUtilisateur::estAdministrateur())) {
            MessageFlash::ajouter("danger", "Vous n'avez pas le droit de créer un administrateur");
            self::redirectionVersURL("controleurFrontal.php?action=afficherFormulaireCreation&controleur=utilisateur");
        }
        elseif($_REQUEST['mdp'] !== $_REQUEST['mdp2']){
            MessageFlash::ajouter("warning", "Les mots de passe ne correspondent pas");
            self::redirectionVersURL("controleurFrontal.php?action=afficherFormulaireCreation&controleur=utilisateur");
        }
        elseif(!filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL)) {
            MessageFlash::ajouter("warning", "Adresse email invalide");
            self::redirectionVersURL("controleurFrontal.php?action=afficherFormulaireCreation&controleur=utilisateur");
        }
        else {
            $utilisateur = self::construireDepuisFormulaire($_REQUEST);
            (new UtilisateurRepository())->ajouter($utilisateur);
            VerificationEmail::envoiEmailValidation($utilisateur);
            MessageFlash::ajouter("success", "Utilisateur créé");
            self::redirectionVersURL("controleurFrontal.php?action=afficherListe&controleur=utilisateur");
        }
    }

    public static function supprimer()
    {
        if(ConnexionUtilisateur::estUtilisateur($_REQUEST['login'])) {
            if (!isset($_REQUEST['login'])) {
                MessageFlash::ajouter("danger", "Erreur avec l'utilisateur " . $_REQUEST['login']);
                self::redirectionVersURL("controleurFrontal.php?action=afficherListe&controleur=utilisateur");
                return;
            }
            $login = $_REQUEST['login'];
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
            if ($utilisateur === null) {
                MessageFlash::ajouter("danger", "Utilisateur " . $login . " non trouvé");
            } else {
                (new UtilisateurRepository())->supprimer(["login" => $login]);
                MessageFlash::ajouter("success", "Utilisateur supprimé");
            }
        } else {
            MessageFlash::ajouter("danger", "Vous n'avez pas le droit de supprimer cet utilisateur");
        }
        self::redirectionVersURL("controleurFrontal.php?action=afficherListe&controleur=utilisateur");
    }

    public static function afficherFormulaireMiseAJour()
    {
        if(ConnexionUtilisateur::estUtilisateur($_REQUEST['login']) || ConnexionUtilisateur::estAdministrateur()) {
            if (!isset($_REQUEST['login'])) {
                MessageFlash::ajouter("warning", "Erreur avec l'utilisateur " . $_REQUEST['login']);
                self::redirectionVersURL("controleurFrontal.php?action=afficherListe&controleur=utilisateur");
                return;
            }
            $login = $_REQUEST['login'];
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
            if ($utilisateur === null) {
                MessageFlash::ajouter("warning", "Utilisateur " . $login . " non trouvé");
                self::redirectionVersURL("controleurFrontal.php?action=afficherListe&controleur=utilisateur");
            } else {
                self::afficherVue('vueGenerale.php', [
                    "titre" => "Mise à jour de l'utilisateur " . $login,
                    "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php",
                    "utilisateur" => $utilisateur,
                ]);
            }
        } else {
            MessageFlash::ajouter("danger", "Vous n'avez pas accès à la modification de cet utilisateur");
            self::redirectionVersURL("controleurFrontal.php?action=afficherListe&controleur=utilisateur");
        }
    }

    public static function mettreAJour() {
        if (!ConnexionUtilisateur::estUtilisateur($_REQUEST['login']) && !ConnexionUtilisateur::estAdministrateur()) {
            MessageFlash::ajouter("danger", "Vous n'avez pas accès à la modification de cet utilisateur");
            self::redirectionVersURL("controleurFrontal.php?action=afficherListe&controleur=utilisateur");
            return;
        }
        if (!isset($_REQUEST['login']) || !isset($_REQUEST['nom']) || !isset($_REQUEST['prenom']) || !isset($_REQUEST['email'])) {
            MessageFlash::ajouter("danger", "Paramètres manquants");
            self::redirectionVersURL("controleurFrontal.php?action=afficherListe&controleur=utilisateur");
            return;
        }
        $utilisateurRepo = new UtilisateurRepository();
        $utilisateur = $utilisateurRepo->recupererParClePrimaire($_REQUEST['login']);
        if ($utilisateur === null) {
            MessageFlash::ajouter("danger", "Utilisateur inconnu");
            self::redirectionVersURL("controleurFrontal.php?action=afficherListe&controleur=utilisateur");
            return;
        }
        if (!MotDePasse::verifier($_REQUEST['ancienMdp'], $utilisateur->getMdpHache()) && !ConnexionUtilisateur::estAdministrateur()) {
            MessageFlash::ajouter("warning", "Ancien mot de passe incorrect");
            self::redirectionVersURL("controleurFrontal.php?action=afficherFormulaireMiseAJour&controleur=utilisateur&login=" . $_REQUEST['login']);
            return;
        }
        if ($_REQUEST['mdp'] !== $_REQUEST['mdp2']) {
            MessageFlash::ajouter("warning", "Les mots de passe ne correspondent pas");
            self::redirectionVersURL("controleurFrontal.php?action=afficherFormulaireMiseAJour&controleur=utilisateur&login=" . $_REQUEST['login']);
            return;
        }
        $utilisateur->setNom($_REQUEST['nom']);
        $utilisateur->setPrenom($_REQUEST['prenom']);
        if (!empty($_REQUEST['mdp'])) {
            $utilisateur->setMdpHache(MotDePasse::hacher($_REQUEST['mdp']));
        }
        if (ConnexionUtilisateur::estAdministrateur() && isset($_REQUEST['estAdmin'])) {
            $utilisateur->setEstAdmin($_REQUEST['estAdmin'] === 'on');
        }
        if ($_REQUEST['email'] !== $utilisateur->getEmail()) {
            if (!filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL)) {
                MessageFlash::ajouter("warning", "Adresse email invalide");
                self::redirectionVersURL("controleurFrontal.php?action=afficherFormulaireMiseAJour&controleur=utilisateur&login=" . $_REQUEST['login']);
                return;
            }
            $utilisateur->setEmailAValider($_REQUEST['email']);
            $utilisateur->setNonce(MotDePasse::genererChaineAleatoire());
            VerificationEmail::envoiEmailValidation($utilisateur);
        }
        $utilisateurRepo->mettreAJour($utilisateur);
        MessageFlash::ajouter("success", "Utilisateur" . $utilisateur->getLogin() . "mis à jour");
        self::redirectionVersURL("controleurFrontal.php?action=afficherListe&controleur=utilisateur");
    }

    public static function afficherFormulaireConnexion() {
        self::afficherVue('vueGenerale.php', [
            "titre" => "Connexion",
            "cheminCorpsVue" => "utilisateur/formulaireConnexion.php",
        ]);
    }

    public static function connecter() {
        if(!isset($_REQUEST['login']) || !isset($_REQUEST['password'])){
            MessageFlash::ajouter("danger", "Paramètres manquants");
            self::redirectionVersURL("controleurFrontal.php?action=afficherFormulaireConnexion&controleur=utilisateur");
        } elseif ((new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']) === null){
            MessageFlash::ajouter("warning", "Utilisateur inconnu");
            self::redirectionVersURL("controleurFrontal.php?action=afficherFormulaireConnexion&controleur=utilisateur");
        }elseif (VerificationEmail::aValideEmail((new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login'])) === false) {
            MessageFlash::ajouter("warning", "Email non validé");
            self::redirectionVersURL("controleurFrontal.php?action=afficherFormulaireConnexion&controleur=utilisateur");
        }elseif (!MotDePasse::verifier($_REQUEST['password'], (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login'])->getMdpHache())){
            MessageFlash::ajouter("warning", "Mot de passe incorrect");
            self::redirectionVersURL("controleurFrontal.php?action=afficherFormulaireConnexion&controleur=utilisateur");
        } else {
            ConnexionUtilisateur::connecter($_REQUEST['login']);
            MessageFlash::ajouter("success", "Connexion réussie");
            self::redirectionVersURL("controleurFrontal.php?action=afficherListe&controleur=utilisateur");
        }
    }

    public static function deconnecter()
    {
        ConnexionUtilisateur::deconnecter();
        MessageFlash::ajouter("success", "Déconnexion réussie");
        self::redirectionVersURL("controleurFrontal.php?action=afficherListe&controleur=utilisateur");
    }

    public static function validerEmail() {
        if(!isset($_REQUEST['login']) || !isset($_REQUEST['nonce'])){
            MessageFlash::ajouter("danger", "Paramètres manquants");
            self::redirectionVersURL("controleurFrontal.php?action=afficherListe&controleur=utilisateur");
        } else {
            if(VerificationEmail::traiterEmailValidation($_REQUEST['login'], $_REQUEST['nonce'])) {
                MessageFlash::ajouter("success", "Email validé");
                self::afficherVue('vueGenerale.php', [
                    "titre" => "Email validé",
                    "cheminCorpsVue" => "utilisateur/detail.php",
                    "utilisateur" => (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']),
                ]);
            } else {
                MessageFlash::ajouter("danger", "Erreur lors de la validation de l'email");
                self::redirectionVersURL("controleurFrontal.php?action=afficherListe&controleur=utilisateur");
            }
        }
    }

    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $login = $tableauDonneesFormulaire['login'];
        $nom = $tableauDonneesFormulaire['nom'];
        $prenom = $tableauDonneesFormulaire['prenom'];
        $mdp = MotDePasse::hacher($tableauDonneesFormulaire['mdp']);
        if(isset($tableauDonneesFormulaire['estAdmin'])) {
            $estAdmin = $tableauDonneesFormulaire['estAdmin'] === 'on';
        } else {
            $estAdmin = false;
        }
        $emailAValider = $tableauDonneesFormulaire['email'];
        return new Utilisateur($login, $nom, $prenom, $mdp , $estAdmin, "", $emailAValider, MotDePasse::genererChaineAleatoire());
    }
}
