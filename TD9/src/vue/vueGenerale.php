<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title><?php use App\Covoiturage\Lib\ConnexionUtilisateur;
        echo $titre; ?></title>
    <link rel="stylesheet" href="../ressources/css/style.css">
    <link rel="stylesheet" href="../ressources/css/navstyles.css">
</head>
<body>

<header>
    <nav>
        <!-- Votre menu de navigation ici -->
        <div>
            <?php
            /** @var string[][] $messagesFlash */
            foreach($messagesFlash as $type => $messagesFlashPourUnType) {
                // $type est l'une des valeurs suivantes : "success", "info", "warning", "danger"
                // $messagesFlashPourUnType est la liste des messages flash d'un type
                foreach ($messagesFlashPourUnType as $messageFlash) {
                    echo <<< HTML
            <div class="alert alert-$type">
               $messageFlash
            </div>
            HTML;
                }
            }
            ?>
        </div>
        <ul>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
            </li>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
            </li>
            <li>
                <a href="controleurFrontal.php?action=afficherFormulairePreference"><img src="../ressources/images/heart.png" alt=""></a>
            </li>
            <?php if(!ConnexionUtilisateur::estConnecte()) { ?>
            <li>
                <a href="controleurFrontal.php?action=afficherFormulaireCreation&controleur=utilisateur"><img src="../ressources/images/add-user.png" alt=""></a>
            </li>
            <li>
                <a href="controleurFrontal.php?action=afficherFormulaireConnexion&controleur=utilisateur"><img src="../ressources/images/enter.png" alt=""></a>
            </li>
            <?php }?>
            <?php if(ConnexionUtilisateur::estConnecte()) { ?>
            <li>
                <a href="controleurFrontal.php?action=afficherDetail&controleur=utilisateur&login=<?= htmlspecialchars(ConnexionUtilisateur::getLoginUtilisateurConnecte())?>"><img src="../ressources/images/user.png" alt=""></a>
            </li>
            <li>
                <a href="controleurFrontal.php?action=deconnecter&controleur=utilisateur"><img src="../ressources/images/logout.png" alt=""></a>
            </li>
            <?php }?>
        </ul>
    </nav>
</header>
<main>
    <?php
    require __DIR__ . "/{$cheminCorpsVue}";
    ?>
</main>
<footer>
    <p>
        Site de covoiturage de Belhaj Yaniss
    </p>
</footer>
</body>
</html>

