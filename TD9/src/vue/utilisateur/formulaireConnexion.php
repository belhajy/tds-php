<form action="controleurFrontal.php" method="<?php use App\Covoiturage\Configuration\ConfigurationSite;

echo ConfigurationSite::getDebug() ? 'get' : 'post' ?>">
    <fieldset>
        <legend>Connexion :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login">Login&#42;</label>
            <input class="InputAddOn-field" type="text" id="login" name="login" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="password">Mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" id="password" name="password" required>
        </p>
        <input type='hidden' name='controleur' value='utilisateur'>
        <input type='hidden' name='action' value='connecter'>
        <p>
            <input class="InputAddOn-field" type="submit" value="Se connecter" />
        </p>
    </fieldset>
</form>