<?php
/** @var Utilisateur $utilisateur */

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Modele\DataObject\Utilisateur;

echo '<p> Utilisateur ' . htmlspecialchars($utilisateur->getNom()) . ' ' . htmlspecialchars($utilisateur->getPrenom()) . ' de login ' . htmlspecialchars($utilisateur->getLogin()) .'.</p>';

if(ConnexionUtilisateur::estConnecte() && ConnexionUtilisateur::estUtilisateur($utilisateur->getLogin())) {
    echo '<p><a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login=' . rawurlencode($utilisateur->getLogin()) . '">' . "Modifier" . '</a>.
          <a href="controleurFrontal.php?controleur=utilisateur&action=supprimer&login=' . rawurlencode($utilisateur->getLogin()) . '">' . "Supprimer" . '</a>.<p>';
}