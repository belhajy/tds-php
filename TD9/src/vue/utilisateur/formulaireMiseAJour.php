<?php
/** @var Utilisateur $utilisateur */

use App\Covoiturage\Configuration\ConfigurationSite;
use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Modele\DataObject\Utilisateur;

?>
<form method="<?php echo ConfigurationSite::getDebug() ? 'get' : 'post' ?>" action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input readonly class="InputAddOn-field" type="text" value="<?= htmlspecialchars($utilisateur->getLogin()) ?>" name="login" id="login_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?= htmlspecialchars($utilisateur->getNom()) ?>" name="nom" id="nom_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prenom&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?= htmlspecialchars($utilisateur->getPrenom()) ?>" name="prenom" id="prenom_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="email_id">Email&#42;</label>
            <input class="InputAddOn-field" type="email" value="<?= htmlspecialchars($utilisateur->getEmail()) ?>" name="email" id="email_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="ancienMdp_id">Ancien mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="ancienMdp" id="ancienMdp_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Nouveau mot de passe</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp" id="mdp_id">
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp2_id">Vérification du nouveau mot de passe</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp2" id="mdp2_id">
        </p>
        <?php if(ConnexionUtilisateur::estAdministrateur()) { ?>
            <p class="InputAddOn">
                <label class="InputAddOn-item" for="estAdmin_id">Administrateur</label>
                <input class="InputAddOn-field" type="checkbox" placeholder="" name="estAdmin" id="estAdmin_id" <?php if ($utilisateur->isEstAdmin()) { echo 'checked';}?>>
            </p>
        <?php } ?>
        <input type='hidden' name='controleur' value='utilisateur'>
        <input type='hidden' name='action' value='mettreAJour'>
        <p>
            <input class="InputAddOn-field" type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>