<?php /** @var Utilisateur[] $conducteurs */

use App\Covoiturage\Modele\DataObject\Utilisateur; ?>
<form method="post" action="controleurFrontal.php">
    <!-- Remplacer method="get" par method="post" pour changer le format d'envoi des données -->
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="depart_id">Depart&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Montpellier" name="depart" id="depart_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="arrivee_id">Arrivée&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Sète" name="arrivee" id="arrivee_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="date_id">Date&#42;</label>
            <input class="InputAddOn-field" type="date" placeholder="JJ/MM/AAAA" name="date" id="date_id"  required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nbPlaces_id">Nombre de places&#42;</label>
            <input class="InputAddOn-field" type="number" placeholder="350" name="nbPlaces" id="nbPlaces_id"  required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prix_id">Prix&#42;</label>
            <input class="InputAddOn-field" type="number" placeholder="20" name="prix" id="prix_id"  required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="conducteurLogin_id">Login du conducteur&#42;</label>
            <select class="InputAddOn-field" name="conducteurLogin" id="conducteurLogin_id" required>
                <?php foreach ($conducteurs as $conducteur): ?>
                    <option value="<?= htmlspecialchars($conducteur->getLogin()) ?>"><?= htmlspecialchars($conducteur->getLogin()) ?></option>
                <?php endforeach; ?>
            </select>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nonFumeur_id">Non Fumeur ?&#42;</label>
            <input class="InputAddOn-field" type="checkbox" placeholder="leblancj" name="nonFumeur" id="nonFumeur_id"/>
        </p>
        <input type='hidden' name='action' value='creerDepuisFormulaire'>
        <input type='hidden' name='controleur' value='trajet'>
        <p>
            <input class="InputAddOn-field" type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>