<?php
require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

use App\Covoiturage\Controleur\ControleurUtilisateur;
use App\Covoiturage\Lib\PreferenceControleur;

// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

// On récupère l'action passée dans l'URL
if (!isset($_REQUEST['controleur'])) {
    if(isset($_COOKIE['preferenceControleur'])) {
        $controleur = PreferenceControleur::lire();
    } else {
        $controleur = 'utilisateur';
    }
} else {
    $controleur = $_REQUEST['controleur'];
}

$nomDeClasseControleur = "App\Covoiturage\Controleur\Controleur" . ucfirst($controleur);
if (isset($_REQUEST['controleur'])) {
    if (!class_exists(ucfirst($nomDeClasseControleur))) {
        ControleurUtilisateur::afficherErreur("Problème avec le nom de la classe passé en paramètre");
    }
}

if (!isset($_REQUEST['action'])) {
    $action = 'afficherListe';
} else {
    $action = $_REQUEST['action'];
    if (!in_array($action, get_class_methods($nomDeClasseControleur))) {
        ControleurUtilisateur::afficherErreur("Problème avec la méthode passée en paramètre");
    }
}
$nomDeClasseControleur::$action();
?>