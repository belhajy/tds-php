<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\ModeleUtilisateur;
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::getUtilisateurs(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php',
            ["titre" => "Liste des utilisateurs",
            "cheminCorpsVue" => "utilisateur/liste.php",
                'utilisateurs' => $utilisateurs]); //appel à la vue
    }

    public static function afficherDetail() : void {
        if(!isset($_GET['login'])){
            self::afficherVue('vueGenerale.php',
            ["titre" => "Erreur avec l'utilisateur " . $_GET['login'],
                "cheminCorpsVue" => "utilisateur/erreur.php",]);
            return;
        }
        $login = $_GET['login'];
        $utilisateur = ModeleUtilisateur::getUtilisateurParLogin($login);
        if($utilisateur === null){
            self::afficherVue('vueGenerale.php',
            ["titre" => "Erreur avec l'utilisateur " . $_GET['login'],
                "cheminCorpsVue" => "utilisateur/erreur.php",]);
        } else {
            self::afficherVue('vueGenerale.php',
                ["titre" => "Detail de l'utilisateur " . $_GET['login'],
                    "cheminCorpsVue" => "utilisateur/detail.php",
                    'utilisateur' => $utilisateur]);
        };
    }

    public static function afficherFormulaireCreation() : void {
        self::afficherVue('vueGenerale.php',
            ["titre" => "Formulaire création utilisateur",
            "cheminCorpsVue" => "utilisateur/formulaireUtilisateur.php",]);
    }

    public static function creerDepuisFormulaire() : void {
        if(!isset($_GET['login']) || !isset($_GET['nom']) || !isset($_GET['prenom'])){
            self::afficherVue('vueGenerale.php', [
                "titre" => "Erreur creation utilisateur",
                "cheminCorpsVue" => "utilisateur/erreur.php",
            ]);
            return;
        }
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];
        $utilisateur = new ModeleUtilisateur($login, $nom, $prenom);
        $utilisateur->ajouter();
        self::afficherVue('vueGenerale.php', [
            "titre" => "Utilisateur créé",
            "cheminCorpsVue" => "utilisateur/utilisateurCree.php",
            "utilisateurs" => ModeleUtilisateur::getUtilisateurs(),
        ]);
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require_once __DIR__ .  "/../vue/$cheminVue"; // Charge la vue
    }
}
?>
