<?php
namespace App\Covoiturage\Modele;

class ModeleUtilisateur {

    private string $login;
    private string $nom;
    private string $prenom;

    /**
     * @var Trajet[]|null
     */
    private ?array $trajetsCommePassager;

    // un getter
    public function getNom() : string {
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom) {
        $this->nom = $nom;
    }

    public function getLogin() : string
    {
        return $this->login;
    }

    public function setLogin(string $login)
    {
        $this->login = $login;
    }

    public function getPrenom() : string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom)
    {
        $this->prenom = $prenom;
    }



    // un constructeur
    public function __construct(
        string $login,
        string $nom,
        string $prenom
   ) {
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->trajetsCommePassager = null;
    }

    public function getTrajetsCommePassager(): ?array
    {
        if($this->trajetsCommePassager === null){
            $this->trajetsCommePassager = $this->recupererTrajetsCommePassager();
        }
        return $this->trajetsCommePassager;
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }



    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString() : string{
        return "ModeleUtilisateur " . $this->nom . " " . $this->prenom . " de login " . $this->login;
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : ModeleUtilisateur {
        return new ModeleUtilisateur($utilisateurFormatTableau['login'], $utilisateurFormatTableau['nom'], $utilisateurFormatTableau['prenom']);
    }

    public static function getUtilisateurs() : array{
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query("SELECT * FROM utilisateur");
        $utilisateurs = array();
        foreach($pdoStatement as $utilisateurFormatTableau){
            $utilisateurs[] = ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
        }
        return $utilisateurs;
    }

    public static function getUtilisateurParLogin(string $login) : ?ModeleUtilisateur {
        $sql = "SELECT * from utilisateur WHERE login = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();
        if($utilisateurFormatTableau === false){
            return null;
        }
        return ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    public function ajouter(): bool
    {
        try {
            $pdo = ConnexionBaseDeDonnees::getPDO();
            $pdoStatement = $pdo->prepare("INSERT INTO utilisateur (login, nom, prenom) VALUES (:login, :nom, :prenom)");
            $values = array(
                "login" => $this->login,
                "nom" => $this->nom,
                "prenom" => $this->prenom,
            );
            $pdoStatement->execute($values);
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }

    /**
     * @return Trajet[]
     */
    private function recupererTrajetsCommePassager() : array {
        $sql = "SELECT * FROM trajet WHERE id IN (SELECT trajetId FROM passager WHERE passagerLogin = :loginTag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "loginTag" => $this->login,
        );
        $pdoStatement->execute($values);
        $trajets = array();
        foreach($pdoStatement as $trajetFormatTableau){
            $trajets[] = Trajet::construireDepuisTableauSQL($trajetFormatTableau);
        }
        return $trajets;
    }



}
?>

