<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        <?php
        $nom = "Schott";
        $prenom = "Liam";
        $login = "liam.schott";
        $tableauLiam = [
            "nom" => $nom,
            "prenom" => $prenom,
            "login" => $login
        ];

        $tableauYaniss = [
            "nom" => "Belhaj",
            "prenom" => "Yaniss",
            "login" => "yaniss.belhaj"
        ];

        $utilsiateurs = [
                1 => $tableauLiam,
                2 => $tableauYaniss
        ];
        if(empty($utilsiateurs)) {
            echo "<p> Il n’y a aucun utilisateur. </p>";
        } else {
            foreach ($utilsiateurs as $cle) {
                echo "<p> Liste des utilisateurs</p>\n";
                echo "<ul>";
                echo "<li> Nom " . $cle["nom"] . "</li>\n";
                echo "<li>Prenom " . $cle["prenom"] . "</li>";
                echo "<li>Login " . $cle["login"] . "</li>";
                echo "</ul>";
            }
        }

        ?>
    </body>
</html> 