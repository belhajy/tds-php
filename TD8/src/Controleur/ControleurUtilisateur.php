<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Lib\VerificationEmail;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
class ControleurUtilisateur extends ControleurGenerique{

    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        ; //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php',
            ["titre" => "Liste des utilisateurs",
            "cheminCorpsVue" => "utilisateur/liste.php",
                'utilisateurs' => $utilisateurs]); //appel à la vue
    }

    public static function afficherDetail() : void {
        if(!isset($_REQUEST['login'])){
            self::afficherErreur("Erreur avec l'utilisateur " . $_REQUEST['login']);
            return;
        }
        $login = $_REQUEST['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if($utilisateur === null){
            self::afficherErreur("Utilisateur " . $login . " non trouvé");
        } else {
            self::afficherVue('vueGenerale.php',
                ["titre" => "Detail de l'utilisateur " .$login,
                    "cheminCorpsVue" => "utilisateur/detail.php",
                    'utilisateur' => $utilisateur]);
        };
    }

    public static function afficherFormulaireCreation() : void {
        self::afficherVue('vueGenerale.php',
            ["titre" => "Formulaire création utilisateur",
            "cheminCorpsVue" => "utilisateur/formulaireUtilisateur.php",]);
    }

    public static function creerDepuisFormulaire() : void {
        if(!isset($_REQUEST['login']) || !isset($_REQUEST['nom']) || !isset($_REQUEST['prenom']) || !isset($_REQUEST['mdp']) || !isset($_REQUEST['mdp2']) || !isset($_REQUEST['email'])){
            self::afficherErreur("Paramètres manquants");
        }
        elseif(isset($_REQUEST['estAdmin']) && ($_REQUEST['estAdmin'] && !ConnexionUtilisateur::estAdministrateur())) {
            self::afficherErreur("Vous n'avez pas le droit de créer un administrateur");
        }
        elseif($_REQUEST['mdp'] !== $_REQUEST['mdp2']){
            self::afficherErreur("Les mots de passe ne correspondent pas");
        }
        elseif(!filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL)) {
            self::afficherErreur("Adresse email invalide");
        }
        else {
            $utilisateur = self::construireDepuisFormulaire($_REQUEST);
            (new UtilisateurRepository())->ajouter($utilisateur);
            VerificationEmail::envoiEmailValidation($utilisateur);
            self::afficherVue('vueGenerale.php', [
                "titre" => "Utilisateur créé",
                "cheminCorpsVue" => "utilisateur/utilisateurCree.php",
                "utilisateurs" => (new UtilisateurRepository())->recuperer(),
            ]);
        }
    }

    public static function afficherErreur(string $messageErreur = "") {
        if(empty($messageErreur)){
            self::afficherVue('vueGenerale.php', [
                "titre" => "Erreur",
                "cheminCorpsVue" => "utilisateur/erreur.php",
                "messageErreur" => "Problème méthode",
            ]);
        } else {
            self::afficherVue('vueGenerale.php', [
                "titre" => "Erreur",
                "cheminCorpsVue" => "utilisateur/erreur.php",
                "messageErreur" => $messageErreur,
            ]);
        }
    }

    public static function supprimer()
    {
        if(ConnexionUtilisateur::estUtilisateur($_REQUEST['login'])) {
            if (!isset($_REQUEST['login'])) {
                self::afficherErreur("Erreur avec l'utilisateur " . $_REQUEST['login']);
                return;
            }
            $login = $_REQUEST['login'];
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
            if ($utilisateur === null) {
                self::afficherErreur("Utilisateur " . $login . " non trouvé");
            } else {
                (new UtilisateurRepository())->supprimer(["login" => $login]);
                self::afficherVue('vueGenerale.php', [
                    "titre" => "Utilisateur supprimé",
                    "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php",
                    "login" => $login,
                    "utilisateurs" => (new UtilisateurRepository())->recuperer(),
                ]);
            }
        } else {
            self::afficherErreur("Vous n'avez pas le droit de supprimer cet utilisateur");
        }
    }

    public static function afficherFormulaireMiseAJour()
    {
        if(ConnexionUtilisateur::estUtilisateur($_REQUEST['login']) || ConnexionUtilisateur::estAdministrateur()) {
            if (!isset($_REQUEST['login'])) {
                self::afficherErreur("Erreur avec l'utilisateur " . $_REQUEST['login']);
                return;
            }
            $login = $_REQUEST['login'];
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
            if ($utilisateur === null) {
                self::afficherErreur("Utilisateur " . $login . " non trouvé");
            } else {
                self::afficherVue('vueGenerale.php', [
                    "titre" => "Mise à jour de l'utilisateur " . $login,
                    "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php",
                    "utilisateur" => $utilisateur,
                ]);
            }
        } else {
            self::afficherErreur("Vous n'avez pas accès à la modification de l'utilisateur " . $_REQUEST['login']);
        }
    }

    public static function mettreAJour() {
        if (!ConnexionUtilisateur::estUtilisateur($_REQUEST['login']) && !ConnexionUtilisateur::estAdministrateur()) {
            self::afficherErreur("Vous n'avez pas accès à la modification de cet utilisateur");
            return;
        }
        if (!isset($_REQUEST['login']) || !isset($_REQUEST['nom']) || !isset($_REQUEST['prenom']) || !isset($_REQUEST['email'])) {
            self::afficherErreur("Paramètres manquants");
            return;
        }
        $utilisateurRepo = new UtilisateurRepository();
        $utilisateur = $utilisateurRepo->recupererParClePrimaire($_REQUEST['login']);
        if ($utilisateur === null) {
            self::afficherErreur("Login inexistant");
            return;
        }
        if (!MotDePasse::verifier($_REQUEST['ancienMdp'], $utilisateur->getMdpHache()) && !ConnexionUtilisateur::estAdministrateur()) {
            self::afficherErreur("Ancien mot de passe incorrect");
            return;
        }
        if ($_REQUEST['mdp'] !== $_REQUEST['mdp2']) {
            self::afficherErreur("Les mots de passe ne correspondent pas");
            return;
        }
        $utilisateur->setNom($_REQUEST['nom']);
        $utilisateur->setPrenom($_REQUEST['prenom']);
        if (!empty($_REQUEST['mdp'])) {
            $utilisateur->setMdpHache(MotDePasse::hacher($_REQUEST['mdp']));
        }
        if (ConnexionUtilisateur::estAdministrateur() && isset($_REQUEST['estAdmin'])) {
            $utilisateur->setEstAdmin($_REQUEST['estAdmin'] === 'on');
        }
        if ($_REQUEST['email'] !== $utilisateur->getEmail()) {
            if (!filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL)) {
                self::afficherErreur("Adresse email invalide");
                return;
            }
            $utilisateur->setEmailAValider($_REQUEST['email']);
            $utilisateur->setNonce(MotDePasse::genererChaineAleatoire());
            VerificationEmail::envoiEmailValidation($utilisateur);
        }
        $utilisateurRepo->mettreAJour($utilisateur);
        self::afficherVue('vueGenerale.php', [
            "titre" => "Utilisateur mis à jour",
            "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php",
            "login" => $utilisateur->getLogin(),
            "utilisateurs" => $utilisateurRepo->recuperer(),
        ]);
    }

    public static function afficherFormulaireConnexion() {
        self::afficherVue('vueGenerale.php', [
            "titre" => "Connexion",
            "cheminCorpsVue" => "utilisateur/formulaireConnexion.php",
        ]);
    }

    public static function connecter() {
        if(!isset($_REQUEST['login']) || !isset($_REQUEST['password'])){
            self::afficherErreur("Paramètres manquants");
        } elseif ((new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']) === null){
            self::afficherErreur("Utilisateur inconnu");
        }elseif (VerificationEmail::aValideEmail((new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login'])) === false) {
            self::afficherErreur("Email non validé");
        }elseif (!MotDePasse::verifier($_REQUEST['password'], (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login'])->getMdpHache())){
            self::afficherErreur("Mot de passe incorrect");
        } else {
            ConnexionUtilisateur::connecter($_REQUEST['login']);
            self::afficherVue('vueGenerale.php', [
                "titre" => "Connexion réussie",
                "cheminCorpsVue" => "utilisateur/utilisateurConnecte.php",
                "login" => $_REQUEST['login'],
                "utilisateurs" => (new UtilisateurRepository())->recuperer(),
            ]);
        }
    }

    public static function deconnecter()
    {
        ConnexionUtilisateur::deconnecter();
        self::afficherVue('vueGenerale.php', [
            "titre" => "Déconnexion",
            "cheminCorpsVue" => "utilisateur/utilisateurDeconnecte.php",
            "utilisateurs" => (new UtilisateurRepository())->recuperer()
        ]);
    }

    public static function validerEmail() {
        if(!isset($_REQUEST['login']) || !isset($_REQUEST['nonce'])){
            self::afficherErreur("Paramètres manquants");
        } else {
            if(VerificationEmail::traiterEmailValidation($_REQUEST['login'], $_REQUEST['nonce'])) {
                self::afficherVue('vueGenerale.php', [
                    "titre" => "Email validé",
                    "cheminCorpsVue" => "utilisateur/detail.php",
                    "utilisateur" => (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']),
                ]);
            } else {
                self::afficherErreur("Erreur lors de la validation de l'email");
            }
        }
    }

    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $login = $tableauDonneesFormulaire['login'];
        $nom = $tableauDonneesFormulaire['nom'];
        $prenom = $tableauDonneesFormulaire['prenom'];
        $mdp = MotDePasse::hacher($tableauDonneesFormulaire['mdp']);
        if(isset($tableauDonneesFormulaire['estAdmin'])) {
            $estAdmin = $tableauDonneesFormulaire['estAdmin'] === 'on';
        } else {
            $estAdmin = false;
        }
        $emailAValider = $tableauDonneesFormulaire['email'];
        return new Utilisateur($login, $nom, $prenom, $mdp , $estAdmin, "", $emailAValider, MotDePasse::genererChaineAleatoire());
    }

    public static function deposerSession() {
        $session = Session::getInstance();
        $session->enregistrer("login", 39276328793);
    }

    public static function lireSession() {
        $session = Session::getInstance();
        echo $session->lire("login");
    }

    public static function supprimerSession() {
        $session = Session::getInstance();
        $session->supprimer("login");
    }

    public static function detruireSession()
    {
        $session = Session::getInstance();
        $session->detruire();
    }
}
