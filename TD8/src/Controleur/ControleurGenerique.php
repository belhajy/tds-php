<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\PreferenceControleur;

class ControleurGenerique
{
    protected static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require_once __DIR__ .  "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulairePreference() {
        self::afficherVue('vueGenerale.php',
            ["titre" => "Formulaire préférence",
                "cheminCorpsVue" => "formulairePreference.php"]);
    }

    public static function enregistrerPreference() {
        if(isset($_REQUEST['controleur_defaut'])) {
            PreferenceControleur::enregistrer($_REQUEST['controleur_defaut']);
            self::afficherVue('vueGenerale.php',
                ["titre" => "Préférence enregistrée",
                    "cheminCorpsVue" => "preferenceEnregistree.php"]);
        }
    }
}