<?php

namespace App\Covoiturage\Modele\DataObject;

class Passager extends AbstractDataObject
{
    private int $trajetId;

    private string $passagerLogin;

    public function __construct(int $trajetId, string $passagerLogin)
    {
        $this->passagerLogin = $passagerLogin;
        $this->trajetId = $trajetId;
    }

    public function getTrajetId(): int
    {
        return $this->trajetId;
    }

    public function setTrajetId(int $trajetId): void
    {
        $this->trajetId = $trajetId;
    }

    public function getPassagerLogin(): string
    {
        return $this->passagerLogin;
    }

    public function setPassagerLogin(string $passagerLogin): void
    {
        $this->passagerLogin = $passagerLogin;
    }



}