<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use DateTime;
use Exception;
use PDOException;

class TrajetRepository extends AbstractRepository
{
    /**
     * @throws Exception
     */
    protected function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]), // À changer
            $trajetTableau["nbPlaces"],
            $trajetTableau["prix"],
            (new UtilisateurRepository())->recupererParClePrimaire($trajetTableau["conducteurLogin"]), // À changer
            $trajetTableau["nonFumeur"], // À changer ?
            []
        );
        self::recupererPassagers($trajet->getId());

        return $trajet;
    }

    /**
     * @return Utilisateur[]
     */
    public static function recupererPassagers($trajetId) : array {
        $pdo = ConnexionBaseDeDonnees::getPDO();

        $sql = "SELECT utilisateur.* 
            FROM utilisateur 
            INNER JOIN passager ON passager.passagerLogin = utilisateur.login 
            WHERE passager.trajetId = :trajetId";

        $pdoStatement = $pdo->prepare($sql);

        $pdoStatement->execute(array('trajetId' => $trajetId));

        $passagers = [];

        foreach ($pdoStatement as $passagerFormatTableau) {
            $passagers[] = (new UtilisateurRepository())->construireDepuisTableauSQL($passagerFormatTableau);
        }

        return $passagers;
    }

    public function supprimerPassager(string $passagerLogin): bool {
        $pdo = ConnexionBaseDeDonnees::getPDO();

        $sql = "DELETE FROM passager WHERE trajetId = :trajetId AND passagerLogin = :passagerLogin";

        $pdoStatement = $pdo->prepare($sql);

        $values = array(
            "trajetId" => $this->id,
            "passagerLogin" => $passagerLogin,
        );

        $pdoStatement->execute($values);

        return $pdoStatement->rowCount() > 0;
    }

    protected function getNomTable(): string
    {
        return "trajet";
    }

    protected function getNomClePrimaire(): string
    {
        return "id";
    }

    protected function getNomsColonnes(): array
    {
        return ["id", "depart", "arrivee", "date", "nbPlaces", "prix", "conducteurLogin", "nonFumeur"];
    }

    protected function formatTableauSQL(AbstractDataObject $trajet): array
    {
        /** @var Trajet $trajet */
        return array(
            "idTag" => $trajet->getId(),
            "departTag" => $trajet->getDepart(),
            "arriveeTag" => $trajet->getArrivee(),
            "dateTag" => $trajet->getDate()->format("Y-m-d"),
            "nbPlacesTag" => $trajet->getNbPlaces(),
            "prixTag" => $trajet->getPrix(),
            "conducteurLoginTag" => $trajet->getConducteur()->getLogin(),
            "nonFumeurTag" => $trajet->isNonFumeur() ? 1 : 0,
        );
    }
}