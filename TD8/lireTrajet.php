<?php

require_once "Trajet.php";

$listeTrajets = Trajet::getTrajets();
foreach ($listeTrajets as $trajet) {
    echo $trajet;

    $passagers = $trajet->getPassagers();
    foreach ($passagers as $passager) {
        echo $passager->getLogin() . " " . $passager->getPrenom() . " " . $passager->getNom();
        echo " <a href='supprimerPassager.php?login=" . urlencode($passager->getLogin()) . "&trajetId=" . urlencode($trajet->getId()) . "'>désinscrire</a><br>";
    }
}